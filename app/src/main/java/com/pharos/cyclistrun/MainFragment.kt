package com.pharos.cyclistrun

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.pharos.cyclistrun.databinding.FragmentMainBinding
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.CONFLATED
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MainFragment : Fragment() {
    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(layoutInflater)

        val channel = Channel<Boolean>(CONFLATED)
        runBlocking {
            channel.send(true)
        }
        var stop = false
        binding.stopButton.setOnClickListener {
            runBlocking {
                channel.send(!stop)
            }
        }

        MainScope().launch {
            val finished = mutableSetOf<View>()
            launch {
                while (true) {
                    stop = channel.receive()
                }
            }
            val childrenSize = binding.cyclistSite.children.count()
            while (finished.size != childrenSize) {
                delay(50)
                if (stop) {
                    binding.cyclistSite.children.forEach {
                        if (moveCyclist(it)) {
                            finished += it
                        }
                    }
                }
            }
            var position = 1
            finished.forEach {
                val textSign = ((it as FrameLayout).children.toList()[1] as TextView)
                textSign.text = position.toString()
                textSign.isVisible = true
                position++
            }
            binding.stopButton.setOnClickListener(null)
            channel.cancel()
            channel.close()
        }


        return binding.root
    }

    private fun moveCyclist(view: View): Boolean {
        ((view as FrameLayout).children.first() as SeekBar).progress += (1..5).random()
        return ((view as FrameLayout).children.first() as SeekBar).progress == 100
    }
}